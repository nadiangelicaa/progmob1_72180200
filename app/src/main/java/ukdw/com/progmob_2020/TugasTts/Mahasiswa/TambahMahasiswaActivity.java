package ukdw.com.progmob_2020.TugasTts.Mahasiswa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class TambahMahasiswaActivity extends AppCompatActivity {

    ProgressDialog pd;
    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_mahasiswa);

        EditText edNamaMhs = (EditText)findViewById(R.id.editTextNamaMhs);
        EditText edNimMhs = (EditText)findViewById(R.id.editTextNimMhs);
        EditText edAlamatMhs = (EditText)findViewById(R.id.editTextAlamatMhs);
        EditText edEmailMhs = (EditText)findViewById(R.id.editTextEmailMhs);
        Button btnTambahMhs = (Button)findViewById(R.id.buttonTambahMhs);
        pd = new ProgressDialog(TambahMahasiswaActivity.this);

        btnTambahMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Please Wait...");
                pd.setCancelable(false);
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        edNamaMhs.getText().toString(),
                        edNimMhs.getText().toString(),
                        edAlamatMhs.getText().toString(),
                        edEmailMhs.getText().toString(),
                        "Random foto",
                        "72180200"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahMahasiswaActivity.this,"Data Mahasiswa Saved!",Toast.LENGTH_LONG).show();
                        //currentLayout = (ConstraintLayout) findViewById(R.id.constraintTambahMhs);
                        //Snackbar.make(currentLayout,"Data Saved!",Snackbar.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(TambahMahasiswaActivity.this,"Failed to Save Data Mahasiswa!",Toast.LENGTH_LONG).show();
                        //currentLayout = (ConstraintLayout) findViewById(R.id.constraintTambahMhs);
                        //Snackbar.make(currentLayout,"Failed to Save Data!",Snackbar.LENGTH_LONG).show();
                    }
                });

                Intent intentAddMhs = new Intent(TambahMahasiswaActivity.this, MainMahasiswaActivity.class);
                startActivity(intentAddMhs);
                finish();
            }
        });


    }
}