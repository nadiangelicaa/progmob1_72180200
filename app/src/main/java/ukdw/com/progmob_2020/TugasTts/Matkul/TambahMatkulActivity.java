package ukdw.com.progmob_2020.TugasTts.Matkul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class TambahMatkulActivity extends AppCompatActivity {

    ProgressDialog pd;
    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_matkul);

        EditText edNamaMatkul = (EditText)findViewById(R.id.editTextNamaMatkul);
        EditText edKodeMatkul = (EditText)findViewById(R.id.editTextKodeMatkul);
        EditText edHariMatkul = (EditText)findViewById(R.id.editTextHariMatkul);
        EditText edSesiMatkul = (EditText)findViewById(R.id.editTextSesiMatkul);
        EditText edSksMatkul = (EditText)findViewById(R.id.editTextSksMatkul);
        Button btnTambahMatkul = (Button)findViewById(R.id.buttonTambahMatkul);
        pd = new ProgressDialog(TambahMatkulActivity.this);

        btnTambahMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Please Wait...");
                pd.setCancelable(false);
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        edNamaMatkul.getText().toString(),
                        "72180200",
                        edKodeMatkul.getText().toString(),
                        Integer.parseInt(edHariMatkul.getText().toString()),
                        Integer.parseInt(edSesiMatkul.getText().toString()),
                        Integer.parseInt(edSksMatkul.getText().toString())
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahMatkulActivity.this,"Data Matkul Saved!",Toast.LENGTH_LONG).show();
                        //currentLayout = (ConstraintLayout) findViewById(R.id.constraintTambahMatkul);
                        //Snackbar.make(currentLayout,"Data Saved!",Snackbar.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(TambahMatkulActivity.this,"Failed to Save Data Matkul!",Toast.LENGTH_LONG).show();
                        //currentLayout = (ConstraintLayout) findViewById(R.id.constraintTambahMatkul);
                        //Snackbar.make(currentLayout,"Failed to Save Data!",Snackbar.LENGTH_LONG).show();
                    }
                });

                Intent intentAddMatkul = new Intent(TambahMatkulActivity.this, MainMatkulActivity.class);
                startActivity(intentAddMatkul);
                finish();
            }
        });
    }
}