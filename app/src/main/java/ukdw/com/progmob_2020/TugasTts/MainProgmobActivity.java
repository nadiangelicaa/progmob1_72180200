package ukdw.com.progmob_2020.TugasTts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import ukdw.com.progmob_2020.Model.User;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.Storage.SharedPrefManager;
import ukdw.com.progmob_2020.TugasTts.Dosen.MainDosenActivity;
import ukdw.com.progmob_2020.TugasTts.Jadwal.MainJadwalActivity;
import ukdw.com.progmob_2020.TugasTts.Mahasiswa.MainMahasiswaActivity;
import ukdw.com.progmob_2020.TugasTts.Matkul.MainMatkulActivity;

public class MainProgmobActivity extends AppCompatActivity {

    private final String TAG = "Main Progmob Activity";

    LinearLayout currentLayout;
    private TextView tvNimNikLogin;
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_progmob);

        //GANTI TITLE BAR
        //getSupportActionBar().setTitle("Welcome to Progmob!");

        //TOOLBAR
        Toolbar toolBarLogout = (Toolbar)findViewById(R.id.toolBarLogout);
        setSupportActionBar(toolBarLogout);

        //untuk nampilin NIM yg login
        tvNimNikLogin = (TextView)findViewById(R.id.tvNimNikLogin);
        User user = SharedPrefManager.getInstance(this).getUser();
        tvNimNikLogin.setText("Hello, " + user.getNimnik() + "!");

        //DOSEN
        ImageView imgDataDosen = (ImageView)findViewById(R.id.imgDataDosen);
        imgDataDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataDosen = new Intent(MainProgmobActivity.this, MainDosenActivity.class);
                startActivity(intentDataDosen);
            }
        });

        TextView txtDataDosen = (TextView)findViewById(R.id.txtDataDosen);
        txtDataDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataDosen = new Intent(MainProgmobActivity.this, MainDosenActivity.class);
                startActivity(intentDataDosen);
            }
        });

        //MAHASISWA
        ImageView imgDataMhs = (ImageView)findViewById(R.id.imgDataMahasiswa);
        imgDataMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataMhs = new Intent(MainProgmobActivity.this, MainMahasiswaActivity.class);
                startActivity(intentDataMhs);
            }
        });

        TextView txtDataMhs = (TextView)findViewById(R.id.txtDataMhs);
        txtDataMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataMhs = new Intent(MainProgmobActivity.this, MainMahasiswaActivity.class);
                startActivity(intentDataMhs);
            }
        });

        //MATKUL
        ImageView imgDataMatkul = (ImageView)findViewById(R.id.imgDataMatkul);
        imgDataMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataMhs = new Intent(MainProgmobActivity.this, MainMatkulActivity.class);
                startActivity(intentDataMhs);
            }
        });

        TextView txtDataMatkul = (TextView)findViewById(R.id.txtDataMatkul);
        txtDataMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataMhs = new Intent(MainProgmobActivity.this, MainMatkulActivity.class);
                startActivity(intentDataMhs);
            }
        });

        //JADWAL
        ImageView imgDataJadwal = (ImageView)findViewById(R.id.imgDataJadwal);
        imgDataJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataMhs = new Intent(MainProgmobActivity.this, MainJadwalActivity.class);
                startActivity(intentDataMhs);
            }
        });

        TextView txtDataJadwal = (TextView)findViewById(R.id.txtDataMatkul);
        txtDataJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataMhs = new Intent(MainProgmobActivity.this, MainJadwalActivity.class);
                startActivity(intentDataMhs);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        //kalau user belum login, diarahkan ke loginactivity
        Log.d(TAG, "User :" + SharedPrefManager.getInstance(this).getUser().getNama());
        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    //MENU LOGOUT
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_logout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuLogout:
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        MainProgmobActivity.this
                );
                builder.setIcon(R.drawable.ic_warning);
                builder.setTitle("Logout");
                builder.setMessage("Do you want to logout?");

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout() {
        SharedPreferences pref = MainProgmobActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        isLogin = pref.getString("isLogin","0");
        editor.putString("isLogin", "0");
        pref.edit().clear().commit();
        Intent intentLogout = new Intent(this, LoginActivity.class);
        intentLogout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogout);
    }

}