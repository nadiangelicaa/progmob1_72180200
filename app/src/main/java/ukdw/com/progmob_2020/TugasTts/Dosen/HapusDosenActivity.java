package ukdw.com.progmob_2020.TugasTts.Dosen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class HapusDosenActivity extends AppCompatActivity {

    ProgressDialog pd;
    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_dosen);

        EditText edNidnHapus = (EditText)findViewById(R.id.editTextNidnHapus);
        Button btnHapusDsn = (Button)findViewById(R.id.buttonHapusDsn);
        pd = new ProgressDialog(HapusDosenActivity.this);

        btnHapusDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Please Wait...");
                pd.setCancelable(false);
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_dsn(
                        edNidnHapus.getText().toString(),
                        "72180200"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusDosenActivity.this, "Data Dosen Deleted!", Toast.LENGTH_LONG).show();
                        //currentLayout = (ConstraintLayout) findViewById(R.id.clHapusDosen);
                        //Snackbar.make(currentLayout,"Data Dosen Deleted!",Snackbar.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusDosenActivity.this, "Failed to Delete Data Dosen!", Toast.LENGTH_LONG).show();
                        //currentLayout = (ConstraintLayout) findViewById(R.id.clHapusDosen);
                        //Snackbar.make(currentLayout,"Failed to Delete Data Dosen!",Snackbar.LENGTH_LONG).show();
                    }
                });

                Intent intentHapusData = new Intent(HapusDosenActivity.this, MainDosenActivity.class);
                startActivity(intentHapusData);
                //finish();
            }
        });
    }
}