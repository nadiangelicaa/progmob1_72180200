package ukdw.com.progmob_2020.TugasTts.Jadwal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.MainProgmobActivity;

public class TambahJadwalActivity extends AppCompatActivity {

    ProgressDialog pd;
    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_jadwal);

        EditText edNamaJadwal = (EditText)findViewById(R.id.editTextNamaJadwal);
        EditText edKodeJadwal = (EditText)findViewById(R.id.editTextKodeJadwal);
        EditText edHariJadwal = (EditText)findViewById(R.id.editTextHariJadwal);
        EditText edSesiJadwal = (EditText)findViewById(R.id.editTextSesiJadwal);
        EditText edSksJadwal = (EditText)findViewById(R.id.editTextSksJadwal);
        Button btnTambahJadwal = (Button)findViewById(R.id.buttonTambahJadwal);
        pd = new ProgressDialog(TambahJadwalActivity.this);

        btnTambahJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Please Wait...");
                pd.setCancelable(false);
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_jadwal(
                        "",
                        edNamaJadwal.getText().toString(),
                        "72180200",
                        edKodeJadwal.getText().toString(),
                        edHariJadwal.getText().toString(),
                        edSesiJadwal.getText().toString(),
                        edSksJadwal.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahJadwalActivity.this,"Data Jadwal Saved!",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(TambahJadwalActivity.this,"Failed to Save Data Jadwal!",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intentAddJadwal = new Intent(TambahJadwalActivity.this, MainProgmobActivity.class);
                startActivity(intentAddJadwal);
            }
        });
    }
}