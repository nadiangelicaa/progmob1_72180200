package ukdw.com.progmob_2020.TugasTts.Mahasiswa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class UpdateMahasiswaActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mahasiswa);

        EditText edNamaBaru = (EditText) findViewById(R.id.editTextNamaMhs);
        EditText edNimBaru = (EditText) findViewById(R.id.editTextNimBaruMhs);
        EditText edNimCari = (EditText) findViewById(R.id.editTextNimCariMhs);
        EditText edAlamatBaru = (EditText) findViewById(R.id.editTextAlamatMhs);
        EditText edEmailBaru = (EditText) findViewById(R.id.editTextEmailMhs);
        Button btnUpdate = (Button) findViewById(R.id.buttonUpdateMhs);
        pd = new ProgressDialog(UpdateMahasiswaActivity.this);

        Intent data = getIntent();

        if (data.getExtras() != null){
            edNamaBaru.setText(data.getStringExtra("nama"));
            edNimBaru.setText(data.getStringExtra("nim"));
            edNimCari.setText(data.getStringExtra("nim"));
            edAlamatBaru.setText(data.getStringExtra("alamat"));
            edEmailBaru.setText(data.getStringExtra("email"));

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Please Wait...");
                    pd.setCancelable(false);
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> call = service.update_mhs(
                            edNamaBaru.getText().toString(),
                            edNimBaru.getText().toString(),
                            edNimCari.getText().toString(),
                            edAlamatBaru.getText().toString(),
                            edEmailBaru.getText().toString(),
                            "",
                            "72180200"
                    );

                    call.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(UpdateMahasiswaActivity.this, "Data Mahasiswa Updated!", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(UpdateMahasiswaActivity.this, "Failed to Update Data Mahasiswa!", Toast.LENGTH_LONG).show();
                        }
                    });

                    Intent intentUpdateMhs = new Intent(UpdateMahasiswaActivity.this, MainMahasiswaActivity.class);
                    startActivity(intentUpdateMhs);
                    finish();
                }
            });
        }
    }
}