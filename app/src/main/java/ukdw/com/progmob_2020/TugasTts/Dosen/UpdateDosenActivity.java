package ukdw.com.progmob_2020.TugasTts.Dosen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class UpdateDosenActivity extends AppCompatActivity {

    ProgressDialog pd;
    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_dosen);

        EditText edNamaDsnBaru = (EditText) findViewById(R.id.editTextNamaDsn);
        EditText edNidnBaru = (EditText) findViewById(R.id.editTextNidnDsn);
        EditText edAlamatDsnBaru = (EditText) findViewById(R.id.editTextAlamatDsn);
        EditText edEmailDsnBaru = (EditText) findViewById(R.id.editTextEmailDsn);
        EditText edGelarBaru = (EditText) findViewById(R.id.editTextGelarDsn);
        EditText edNidnCari = (EditText) findViewById(R.id.editTextNidnCariDsn);
        Button btnUpdateDsn = (Button) findViewById(R.id.buttonUpdateDsn);
        pd = new ProgressDialog(UpdateDosenActivity.this);

        Intent data = getIntent();

        if (data.getExtras() != null){
            edNamaDsnBaru.setText(data.getStringExtra("nama"));
            edNidnBaru.setText(data.getStringExtra("nidn"));
            edAlamatDsnBaru.setText(data.getStringExtra("alamat"));
            edEmailDsnBaru.setText(data.getStringExtra("email"));
            edGelarBaru.setText(data.getStringExtra("gelar"));
            edNidnCari.setText(data.getStringExtra("nidn"));

            btnUpdateDsn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Please Wait...");
                    pd.setCancelable(false);
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> call = service.update_dsn(
                            edNamaDsnBaru.getText().toString(),
                            edNidnBaru.getText().toString(),
                            edAlamatDsnBaru.getText().toString(),
                            edEmailDsnBaru.getText().toString(),
                            edGelarBaru.getText().toString(),
                            "",
                            "72180200",
                            edNidnCari.getText().toString()
                    );

                    call.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(UpdateDosenActivity.this, "Data Dosen Updated!", Toast.LENGTH_LONG).show();
                            //currentLayout = (ConstraintLayout) findViewById(R.id.clUpdateDosen);
                            //Snackbar.make(currentLayout,"Data Dosen Updated!",Snackbar.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(UpdateDosenActivity.this, "Failed to Update Data Dosen!", Toast.LENGTH_LONG).show();
                            //currentLayout = (ConstraintLayout) findViewById(R.id.clUpdateDosen);
                            //Snackbar.make(currentLayout,"Failed to Update Data Dosen!",Snackbar.LENGTH_LONG).show();
                        }
                    });

                    Intent intentUpdateDsn = new Intent(UpdateDosenActivity.this, MainDosenActivity.class);
                    startActivity(intentUpdateDsn);
                    //finish();
                }
            });
        }
    }
}