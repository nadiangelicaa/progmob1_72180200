package ukdw.com.progmob_2020.TugasTts.Matkul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class UpdateMatkulActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_matkul);

        EditText edNamaMatkulBaru = (EditText) findViewById(R.id.editTextNamaMk);
        EditText edKodeMatkulBaru = (EditText) findViewById(R.id.editTextKodeMk);
        EditText edHariBaru = (EditText) findViewById(R.id.editTextHariMk);
        EditText edSesiBaru = (EditText) findViewById(R.id.editTextSesiMk);
        EditText edSksBaru = (EditText) findViewById(R.id.editTextSksMk);
        EditText edKodeMatkulCari = (EditText) findViewById(R.id.editTextKodeMkCari);
        Button btnUpdate = (Button) findViewById(R.id.buttonUpdateMk);
        pd = new ProgressDialog(UpdateMatkulActivity.this);

        Intent data = getIntent();

        if (data.getExtras() != null){
            edNamaMatkulBaru.setText(data.getStringExtra("nama"));
            edKodeMatkulBaru.setText(data.getStringExtra("kode"));
            edHariBaru.setText(data.getStringExtra("hari"));
            edSesiBaru.setText(data.getStringExtra("sesi"));
            edSksBaru.setText(data.getStringExtra("sks"));
            edKodeMatkulCari.setText(data.getStringExtra("kode"));

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Please Wait...");
                    pd.setCancelable(false);
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> call = service.update_matkul(
                            edNamaMatkulBaru.getText().toString(),
                            "72180200",
                            edKodeMatkulBaru.getText().toString(),
                            Integer.parseInt(edHariBaru.getText().toString()),
                            Integer.parseInt(edSesiBaru.getText().toString()),
                            Integer.parseInt(edSksBaru.getText().toString()),
                            edKodeMatkulCari.getText().toString()
                    );

                    call.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(UpdateMatkulActivity.this, "Data Matkul Updated!", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(UpdateMatkulActivity.this, "Failed to Update Data Matkul!", Toast.LENGTH_LONG).show();
                        }
                    });

                    Intent intentUpdateMatkul = new Intent(UpdateMatkulActivity.this, MainMatkulActivity.class);
                    startActivity(intentUpdateMatkul);
                    finish();
                }
            });
        }
    }
}