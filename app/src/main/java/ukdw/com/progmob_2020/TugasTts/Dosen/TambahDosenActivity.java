package ukdw.com.progmob_2020.TugasTts.Dosen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class TambahDosenActivity extends AppCompatActivity {

    ProgressDialog pd;
    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_dosen);

        EditText edNamaDsn = (EditText)findViewById(R.id.editTextNamaDosen);
        EditText edNidnDsn = (EditText)findViewById(R.id.editTextNidnDosen);
        EditText edAlamatDsn = (EditText)findViewById(R.id.editTextAlamatDosen);
        EditText edEmailDsn = (EditText)findViewById(R.id.editTextEmailDosen);
        EditText edGelarDsn = (EditText)findViewById(R.id.editTextGelarDosen);
        Button btnTambahDsn = (Button)findViewById(R.id.buttonTambahDosen);
        pd = new ProgressDialog(TambahDosenActivity.this);

        btnTambahDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Please Wait...");
                pd.setCancelable(false);
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dsn(
                        edNamaDsn.getText().toString(),
                        edNidnDsn.getText().toString(),
                        edAlamatDsn.getText().toString(),
                        edEmailDsn.getText().toString(),
                        edGelarDsn.getText().toString(),
                        "Random foto",
                        "72180200"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahDosenActivity.this,"Data Dosen Saved!",Toast.LENGTH_LONG).show();
                        //currentLayout = (ConstraintLayout) findViewById(R.id.constraintTambahDsn);
                        //Snackbar.make(currentLayout,"Data Dosen Saved!",Snackbar.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(TambahDosenActivity.this,"Failed to Save Data Dosen!",Toast.LENGTH_LONG).show();
                        //currentLayout = (ConstraintLayout) findViewById(R.id.constraintTambahDsn);
                        //Snackbar.make(currentLayout,"Failed to Save Data Dosen!",Snackbar.LENGTH_LONG).show();
                    }
                });

                Intent intentAddDsn = new Intent(TambahDosenActivity.this, MainDosenActivity.class);
                startActivity(intentAddDsn);
                //finish();
            }
        });

    }
}