package ukdw.com.progmob_2020.TugasTts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import ukdw.com.progmob_2020.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // langsung pindah ke LoginActivity begitu memasuki splash screen
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
        finish();
    }
}