package ukdw.com.progmob_2020.TugasTts.Mahasiswa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.MhsRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.MainProgmobActivity;

public class MainMahasiswaActivity extends AppCompatActivity {

    //dibuat global karna akan dipanggil scr asynchronous
    RecyclerView rvMhs;
    MhsRecyclerAdapter mhsAdapter;
    ProgressDialog pd;
    List<Mahasiswa> mahasiswaList;

    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mahasiswa);

        //TOOLBAR
        Toolbar toolBarTambahMhs = (Toolbar)findViewById(R.id.toolbarTambahMahasiswa);
        setSupportActionBar(toolBarTambahMhs);

        //GANTI TITLE BAR
        //getSupportActionBar().setTitle("Data Mahasiswa");
        toolBarTambahMhs.setNavigationIcon(R.drawable.ic_back);
        toolBarTambahMhs.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTambah = new Intent(MainMahasiswaActivity.this, MainProgmobActivity.class);
                intentTambah.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentTambah);
                //finish();
            }
        });

        rvMhs = (RecyclerView)findViewById(R.id.rvDataMahasiswa);
        pd = new ProgressDialog(this);
        pd.setTitle("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Mahasiswa>> call = service.getMahasiswa("72180200");

        call.enqueue(new Callback<List<Mahasiswa>>() {
            @Override
            public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                pd.dismiss();
                mahasiswaList = response.body();
                mhsAdapter = new MhsRecyclerAdapter(mahasiswaList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainMahasiswaActivity.this);
                rvMhs.setLayoutManager(layoutManager);
                rvMhs.setAdapter(mhsAdapter);
            }

            @Override
            public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainMahasiswaActivity.this,"Error!",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tambah_mhs, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tambahMahasiswa:
                //currentLayout = (ConstraintLayout) findViewById(R.id.constraintLayoutMhs);
                //Snackbar.make(currentLayout,"Tambah Data Mahasiswa",Snackbar.LENGTH_LONG).show();
                Intent intentTambah = new Intent(this, TambahMahasiswaActivity.class);
                startActivity(intentTambah);
                return true;
            case R.id.hapusMahasiswa:
                Intent intentHapus = new Intent(this,HapusMahasiswaActivity.class);
                startActivity(intentHapus);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}