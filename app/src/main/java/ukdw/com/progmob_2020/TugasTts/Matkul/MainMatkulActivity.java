package ukdw.com.progmob_2020.TugasTts.Matkul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.MatkulRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.MainProgmobActivity;

public class MainMatkulActivity extends AppCompatActivity {

    //dibuat global karna akan dipanggil scr asynchronous
    RecyclerView rvMatkul;
    MatkulRecyclerAdapter matkulAdapter;
    ProgressDialog pd;
    List<Matkul> matkulList;

    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_matkul);

        //TOOLBAR
        Toolbar toolBarTambahMatkul = (Toolbar)findViewById(R.id.toolbarTambahMatkul);
        setSupportActionBar(toolBarTambahMatkul);

        //GANTI TITLE BAR
        //getSupportActionBar().setTitle("Data Matakuliah");
        toolBarTambahMatkul.setNavigationIcon(R.drawable.ic_back);
        toolBarTambahMatkul.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTambah = new Intent(MainMatkulActivity.this, MainProgmobActivity.class);
                intentTambah.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentTambah);
                //finish();
            }
        });

        rvMatkul = (RecyclerView)findViewById(R.id.rvDataMatkul);
        pd = new ProgressDialog(this);
        pd.setTitle("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matkul>> call = service.getMatkul("72180200");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matkulList = response.body();
                matkulAdapter = new MatkulRecyclerAdapter(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainMatkulActivity.this);
                rvMatkul.setLayoutManager(layoutManager);
                rvMatkul.setAdapter(matkulAdapter);
            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainMatkulActivity.this,"Error!",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tambah_matkul, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tambahMatkul:
                currentLayout = (ConstraintLayout) findViewById(R.id.constraintLayoutMatkul);
                //Snackbar.make(currentLayout,"Tambah Data Matakuliah",Snackbar.LENGTH_LONG).show();
                Intent intentTambah = new Intent(this, TambahMatkulActivity.class);
                startActivity(intentTambah);
                return true;
            case R.id.hapusMatkul:
                Intent intentHapus = new Intent(this, HapusMatkulActivity.class);
                startActivity(intentHapus);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}