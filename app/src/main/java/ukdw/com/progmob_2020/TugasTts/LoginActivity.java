package ukdw.com.progmob_2020.TugasTts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Model.LoginResponse;
import ukdw.com.progmob_2020.Model.User;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.Storage.SharedPrefManager;

public class LoginActivity extends AppCompatActivity {

    private final String TAG = "Login Activity";

    private EditText edNimLogin, edPassword;
    private Button btnLogin;
    ProgressDialog pd;
    ConstraintLayout currentLayout;
    String isLogin="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        /* Menginisialisasi variable dari Layout LoginActivity */
        edNimLogin = (EditText)findViewById(R.id.editTextNim);
        edPassword = (EditText)findViewById(R.id.editTextPassword);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        pd = new ProgressDialog(LoginActivity.this);

        SharedPreferences pref = LoginActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        //agar bs mengisi nilai pref
        SharedPreferences.Editor editor = pref.edit();
        isLogin = pref.getString("isLogin","0");
        if(isLogin.equals("1")){
            Intent intent = new Intent(LoginActivity.this, MainProgmobActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //harus cek dlu kalau nim/passwordnya benar/salah
                userLogin();
                editor.putString("isLogin","1");
                editor.commit();
            }
        });
    }

    private void userLogin(){
        String nimnik = edNimLogin.getText().toString();
        String password = edPassword.getText().toString();

        //validation nim
        if(nimnik.isEmpty()){
            edNimLogin.setError("NIM is Required!");
            edNimLogin.requestFocus();
            return;
        }
        if(nimnik.length() < 8){
            edNimLogin.setError("NIM Should be 8 Character!");
            edNimLogin.requestFocus();
            return;
        }else if(nimnik.length() > 8){
            edNimLogin.setError("NIM Should be 8 Character!");
            edNimLogin.requestFocus();
            return;
        }

        //validation password
        if(password.isEmpty()){
            edPassword.setError("Password is Required!");
            edPassword.requestFocus();
            return;
        }

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<User[]> call = service.login_admin_array(
                edNimLogin.getText().toString(),
                edPassword.getText().toString()
        );

        call.enqueue(new Callback<User[]>() {
            @Override
            public void onResponse(Call<User[]> call, Response<User[]> response) {
                User[] users = response.body();
                if(users.length > 0){
                    //proceed with the login karena users masih blm ada isinya brti msh blm login
                    //save user to shared preferences
                    //kalau data user[0] tdk kosong, maka bisa login
                    /*if (users[0] != null) {
                        Log.d(TAG, "loginResponse[0] found " + users[0].getNama());
                    }else{
                        Toast.makeText(LoginActivity.this, "NIM/ Password Invalid" + response.message(), Toast.LENGTH_LONG).show();
                    }*/
                    SharedPrefManager.getInstance(LoginActivity.this).saveUser(users[0]);
                    Toast.makeText(LoginActivity.this, "Login Succeed!" + response.message(), Toast.LENGTH_LONG).show();
                    //currentLayout = (ConstraintLayout)findViewById(R.id.constraintLayoutLogin);
                    //Snackbar.make(currentLayout,"Login Succeed!",Snackbar.LENGTH_LONG).show();
                    //open new activity
                    Intent intent = new Intent(LoginActivity.this, MainProgmobActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    //KALAU LOGIN ERROR KARENA NIM/PASSWORD SALAH
                    //Toast.makeText(LoginActivity.this, "Login Failed! Invalid NIM/Password!" + response.message(), Toast.LENGTH_LONG).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            LoginActivity.this
                    );
                    builder.setIcon(R.drawable.ic_warning);
                    builder.setTitle("Login Failed");
                    builder.setMessage("Your NIM/ Password is Invalid!");

                    builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    edNimLogin.setText("");
                    edPassword.setText("");
                    edNimLogin.requestFocus();
                    return;
                }
            }

            @Override
            public void onFailure(Call<User[]> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Login Failed!", Toast.LENGTH_LONG).show();
                Log.e(TAG, t.getMessage());
            }
        });
    }

    //yang selalu dijalankan pertama
    @Override
    protected void onStart() {
        super.onStart();
        //kalau user sudah login
        /*if(SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(LoginActivity.this, MainProgmobActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }*/
    }


}


