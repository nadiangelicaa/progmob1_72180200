package ukdw.com.progmob_2020.TugasTts.Jadwal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.MainProgmobActivity;

public class MainJadwalActivity extends AppCompatActivity {

    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_jadwal);

        //TOOLBAR
        Toolbar toolBarTambahJadwal = (Toolbar)findViewById(R.id.toolbarTambahJadwal);
        setSupportActionBar(toolBarTambahJadwal);

        //GANTI TITLE BAR
        //getSupportActionBar().setTitle("Data Jadwal");
        toolBarTambahJadwal.setNavigationIcon(R.drawable.ic_back);
        toolBarTambahJadwal.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTambah = new Intent(MainJadwalActivity.this, MainProgmobActivity.class);
                intentTambah.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentTambah);
                //finish();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(
                MainJadwalActivity.this
        );
        builder.setIcon(R.drawable.ic_error);
        builder.setTitle("Not Available");
        builder.setMessage("Can't Access Data Jadwal!");

        builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //dialog.cancel();
                Intent intent = new Intent(MainJadwalActivity.this, MainProgmobActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tambah_jadwal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*@Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tambahJadwal:
                currentLayout = (ConstraintLayout) findViewById(R.id.constraintLayoutJadwal);
                //Snackbar.make(currentLayout,"Tambah Data Jadwal",Snackbar.LENGTH_LONG).show();
                Intent intent = new Intent(this, TambahJadwalActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
}