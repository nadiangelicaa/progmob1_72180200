package ukdw.com.progmob_2020.TugasTts.Dosen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.DsnRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.MainProgmobActivity;

public class MainDosenActivity extends AppCompatActivity {

    //dibuat global karna akan dipanggil scr asynchronous
    RecyclerView rvDsn;
    DsnRecyclerAdapter dsnAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;

    ConstraintLayout currentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        Toolbar toolBarTambahDsn = (Toolbar)findViewById(R.id.toolbarTambahDosen);
        setSupportActionBar(toolBarTambahDsn);

        //GANTI TITLE BAR
        //getSupportActionBar().setTitle("Data Dosen");
        //toolBarTambahDsn.setLogo(R.drawable.ic_back);
        toolBarTambahDsn.setNavigationIcon(R.drawable.ic_back);
        toolBarTambahDsn.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTambah = new Intent(MainDosenActivity.this, MainProgmobActivity.class);
                intentTambah.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentTambah);
                //finish();
            }
        });

        rvDsn = (RecyclerView)findViewById(R.id.rvDataDosen);
        pd = new ProgressDialog(this);
        pd.setTitle("Please Wait...");
        pd.setCancelable(false);
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180200");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                dsnAdapter = new DsnRecyclerAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainDosenActivity.this);
                rvDsn.setLayoutManager(layoutManager);
                rvDsn.setAdapter(dsnAdapter);
            }

            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainDosenActivity.this,"Error!",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the tool bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tambah_dsn, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tambahDosen:
                //currentLayout = (ConstraintLayout) findViewById(R.id.constraintLayoutDsn);
                //Snackbar.make(currentLayout,"Tambah Data Dosen",Snackbar.LENGTH_LONG).show();
                Intent intentTambah = new Intent(this, TambahDosenActivity.class);
                startActivity(intentTambah);
                return true;
            case R.id.hapusDosen:
                Intent intentHapus = new Intent(this, HapusDosenActivity.class);
                startActivity(intentHapus);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}