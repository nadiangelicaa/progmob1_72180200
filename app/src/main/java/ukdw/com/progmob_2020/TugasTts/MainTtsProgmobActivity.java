package ukdw.com.progmob_2020.TugasTts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import ukdw.com.progmob_2020.Adapter.MainTtsProgmobAdapter;
import ukdw.com.progmob_2020.Crud.MainMhsActivity;
import ukdw.com.progmob_2020.R;

public class MainTtsProgmobActivity extends AppCompatActivity {
    GridView gvMainTtsProgmob;
    String[] numberWord = {"Data Mahasiswa","Data Dosen","Data Matakuliah","Data Jadwal"};

    int[] numberImage = {R.drawable.data_mahasiswa,R.drawable.data_dosen,
                        R.drawable.data_matkul,R.drawable.data_jadwal};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tts_progmob);

        gvMainTtsProgmob = (GridView)findViewById(R.id.gvMainTtsProgmob);

        MainTtsProgmobAdapter adapterMainTtsProgmob = new MainTtsProgmobAdapter(MainTtsProgmobActivity.this,numberWord,numberImage);
        gvMainTtsProgmob.setAdapter(adapterMainTtsProgmob);

        gvMainTtsProgmob.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "You Clicked " + numberWord[+position],
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    
}