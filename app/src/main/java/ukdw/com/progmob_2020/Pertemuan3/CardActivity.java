package ukdw.com.progmob_2020.Pertemuan3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Adapter.MahasiswaCardAdapter;
import ukdw.com.progmob_2020.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;

public class CardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        //deklarasi
        RecyclerView cv = (RecyclerView)findViewById(R.id.rvCardView);
        MahasiswaCardAdapter mahasiswaCardAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Jong","72180200","081578815358");
        Mahasiswa m2 = new Mahasiswa("Julia","72180201","085743747650");
        Mahasiswa m3 = new Mahasiswa("Kuncoro","72180202","08578875335");
        Mahasiswa m4 = new Mahasiswa("Nugroho","72180203","089878775888");
        Mahasiswa m5 = new Mahasiswa("Daniel","72180204","087743456509");

        //masukkan data baru kedalam list
        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaCardAdapter = new MahasiswaCardAdapter(CardActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList); //memasukkan list yg ada disini ke dalam adapter

        cv.setLayoutManager(new LinearLayoutManager(CardActivity.this));
        cv.setAdapter(mahasiswaCardAdapter);
    }
}