package ukdw.com.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class PrefActivity extends AppCompatActivity {

    //jadikan global utk var isLogin nya, default valuenya kosong
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        final Button btnPref = (Button)findViewById(R.id.btnPref);

        final SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        //agar bs mengisi nilai pref
        final SharedPreferences.Editor editor = pref.edit();
        //EDITOR ADALAH TOOLS DARI SHAREDPREFERENCES UTK MELAKUKAN EDIT THDP VALUENYA

        //MEMBACA PREF LOGIN APAKAH TRUE/ FALSE
        isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1")){
            //karena sudah bisa login, maka tombolnya diset jadi logout
            btnPref.setText("Logout");
        }else{
            //kalo nilai isLogin=0 (default valuesnya) brti blm login, maka tombolnya diset login
            btnPref.setText("Login");
        }

        //PENGISIAN PREF
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLogin = pref.getString("isLogin", "0");
                //KALAU BELUM LOGIN
                if (isLogin.equals("0")){
                    //ketika klik btnLogin, maka nilai isLogin diubah jadi 1
                    editor.putString("isLogin","1");
                    //saat udh login, button diubah jadi logout
                    btnPref.setText("Logout");
                }else{
                    //ketika udh login, kalau pencet logout, maka nilai isLogin diubah jadi 0
                    editor.putString("isLogin","0");
                    //saat udh logout, button diubah jadi login lagi
                    btnPref.setText("Login");
                }
                editor.commit(); //menyimpan perubahan yg tjd di preference, pref=session
            }
        });

    }
}