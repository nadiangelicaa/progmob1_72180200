package ukdw.com.progmob_2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Jadwal {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("matkul")
    @Expose
    private String matkul;

    @SerializedName("dosen")
    @Expose
    private String dosen;

    @SerializedName("nidn")
    @Expose
    private String nidn;

    @SerializedName("hari")
    @Expose
    private String hari;

    @SerializedName("sesi")
    @Expose
    private String sesi;

    @SerializedName("sks")
    @Expose
    private String sks;

    //KONSTRUKTOR DGN ID
    public Jadwal(String id, String matkul, String dosen, String nidn, String hari, String sesi, String sks) {
        this.id = id;
        this.matkul = matkul;
        this.dosen = dosen;
        this.nidn = nidn;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
    }

    //KONSTRUKTOR TANPA ID
    public Jadwal(String matkul, String dosen, String nidn, String hari, String sesi, String sks) {
        this.matkul = matkul;
        this.dosen = dosen;
        this.nidn = nidn;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
    }

    //SETTER DAN GETTER SEMUA VARIABEL
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMatkul() {
        return matkul;
    }

    public void setMatkul(String matkul) {
        this.matkul = matkul;
    }

    public String getDosen() {
        return dosen;
    }

    public void setDosen(String dosen) {
        this.dosen = dosen;
    }

    public String getNidn() {
        return nidn;
    }

    public void setNidn(String nidn) {
        this.nidn = nidn;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getSesi() {
        return sesi;
    }

    public void setSesi(String sesi) {
        this.sesi = sesi;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }
}
