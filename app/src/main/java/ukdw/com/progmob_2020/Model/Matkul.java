package ukdw.com.progmob_2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Matkul {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("kode")
    @Expose
    private String kode;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("hari")
    @Expose
    private Integer hari;

    @SerializedName("sesi")
    @Expose
    private Integer sesi;

    @SerializedName("sks")
    @Expose
    private Integer sks;

    //KONSTRUKTOR DGN ID
    public Matkul(String id, String kode, String nama, Integer hari, Integer sesi, Integer sks) {
        this.id = id;
        this.kode = kode;
        this.nama = nama;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
    }

    //KONSTRUKTOR TANPA ID
    public Matkul(String kode, String nama, Integer hari, Integer sesi, Integer sks) {
        this.kode = kode;
        this.nama = nama;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
    }

    //SETTER DAN GETTER SEMUA VARIABEL
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getHari() {
        return hari;
    }

    public void setHari(Integer hari) {
        this.hari = hari;
    }

    public Integer getSesi() {
        return sesi;
    }

    public void setSesi(Integer sesi) {
        this.sesi = sesi;
    }

    public Integer getSks() {
        return sks;
    }

    public void setSks(Integer sks) {
        this.sks = sks;
    }
}
