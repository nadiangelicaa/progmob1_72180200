package ukdw.com.progmob_2020.Network;

import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Model.Jadwal;
import ukdw.com.progmob_2020.Model.LoginResponse;
import ukdw.com.progmob_2020.Model.Mahasiswa;


import java.util.List;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.Model.User;

public interface GetDataService {
    //Handling array response
    @FormUrlEncoded
    @POST("api/progmob/login")
    Call<User[]> login_admin_array(
            @Field("nimnik") String nimnik,
            @Field("password") String password
    );

    //MAHASISWA-READ
    @GET("api/progmob/mhs/{nim_progmob}")
    Call<List<Mahasiswa>> getMahasiswa(@Path("nim_progmob") String nim_progmob);

    //tambahan utk lihat data nim tertentu yg dicari
    /*@GET("api/progmob/mhs/{nim_progmob}/{nim_mhs}")
    Call<DefaultResult> get_mhs_nim_dicari(
            @Field("nim") String nim,
            @Field("nim_progmob") String nim_progmob
    );*/

    //MAHASISWA-DELETE
    @FormUrlEncoded
    @POST("api/progmob/mhs/delete")
    Call<DefaultResult>delete_mhs(
            @Field("nim") String id,
            @Field("nim_progmob") String nim_progmob
    );

    //MAHASISWA-CREATE
    @FormUrlEncoded
    @POST("api/progmob/mhs/create")
    Call<DefaultResult> add_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    //MAHASISWA UPDATE
    @FormUrlEncoded
    @POST("api/progmob/mhs/update")
    Call<DefaultResult>update_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("nim_cari") String nim_cari,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    //DOSEN - READ BY NIM PROGMOB
    @GET("api/progmob/dosen/{nim_progmob}")
    Call<List<Dosen>> getDosen(@Path("nim_progmob") String nim_progmob);

    //DOSEN - DELETE
    @FormUrlEncoded
    @POST("api/progmob/dosen/delete")
    Call<DefaultResult>delete_dsn(
            @Field("nidn") String nidn,
            @Field("nim_progmob") String nim_progmob
    );

    //DOSEN - CREATE
    @FormUrlEncoded
    @POST("api/progmob/dosen/create")
    Call<DefaultResult> add_dsn(
            @Field("nama") String nama,
            @Field("nidn") String nidn,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("gelar") String gelar,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    //DOSEN - UPDATE
    @FormUrlEncoded
    @POST("api/progmob/dosen/update")
    Call<DefaultResult>update_dsn(
            @Field("nama") String nama,
            @Field("nidn") String nidn,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("gelar") String gelar,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob,
            @Field("nidn_cari") String nidn_cari
    );

    //MATKUL - READ
    @GET("api/progmob/matkul/{nim_progmob}")
    Call<List<Matkul>> getMatkul(@Path("nim_progmob") String nim_progmob);

    //MATKUL - DELETE
    @FormUrlEncoded
    @POST("api/progmob/matkul/delete")
    Call<DefaultResult>delete_matkul(
            @Field("kode") String kode,
            @Field("nim_progmob") String nim_progmob
    );

    //MATKUL - CREATE
    @FormUrlEncoded
    @POST("api/progmob/matkul/create")
    Call<DefaultResult> add_matkul(
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") Integer hari,
            @Field("sesi") Integer sesi,
            @Field("sks") Integer sks
    );

    //MATKUL - UPDATE
    @FormUrlEncoded
    @POST("api/progmob/matkul/update")
    Call<DefaultResult> update_matkul(
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") Integer hari,
            @Field("sesi") Integer sesi,
            @Field("sks") Integer sks,
            @Field("kode_cari") String kode_cari
    );

    //JADWAL - READ
    @GET("api/progmob/jadwal/{nim_progmob}")
    Call<List<Jadwal>> getJadwal(@Path("nim_progmob") String nim_progmob);

    //JADWAL - DELETE
    @FormUrlEncoded
    @POST("api/progmob/jadwal/delete")
    Call<DefaultResult>delete_jadwal(
            @Field("id") String id,
            @Field("nim_progmob") String nim_progmob
    );

    //JADWAL - CREATE
    @FormUrlEncoded
    @POST("api/progmob/jadwal/create")
    Call<DefaultResult> add_jadwal(
            @Field("id") String id,
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") String hari,
            @Field("sesi") String sesi,
            @Field("sks") String sks
    );

    //JADWAL - UPDATE
    @FormUrlEncoded
    @POST("api/progmob/jadwal/update")
    Call<DefaultResult> update_jadwal(
            @Field("id") String id,
            @Field("id_dosen") String id_dosen,
            @Field("id_matkul") String id_matkul,
            @Field("nim_progmob") String nim_progmob
    );
}