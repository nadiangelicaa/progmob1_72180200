package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.Mahasiswa.UpdateMahasiswaActivity;
import ukdw.com.progmob_2020.TugasTts.Matkul.UpdateMatkulActivity;

public class MatkulRecyclerAdapter extends RecyclerView.Adapter<MatkulRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Matkul> matkulList;

    public MatkulRecyclerAdapter(Context context) {
        this.context = context;
        matkulList = new ArrayList<>();
    }

    //tambah konstruktor
    public MatkulRecyclerAdapter(List<Matkul> matkulList){
        this.matkulList = matkulList;
    }

    public List<Matkul> getMatkulList() {
        return matkulList;
    }

    public void setMatkulList(List<Matkul> matkulList) {
        this.matkulList = matkulList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_matkul,parent,false);
        return new ViewHolder(v); //utk menampilkan viewholder di dlm recyclerview nya
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Matkul mk = matkulList.get(position);
        holder.tvKodeMatkul.setText(mk.getKode());
        holder.tvNamaMatkul.setText(mk.getNama());
        //hari,sesi,sks tipenya int diubah jadi string, utk update & insert nanti parse lagi dlu jadiin int lagi
        holder.tvHariMatkul.setText(String.valueOf(mk.getHari()));
        holder.tvSesiMatkul.setText(String.valueOf(mk.getSesi()));
        holder.tvSksMatkul.setText(String.valueOf(mk.getSesi()));
        holder.mk = mk;
    }

    @Override
    public int getItemCount() {
        //return 0;
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvKodeMatkul, tvNamaMatkul, tvHariMatkul, tvSesiMatkul, tvSksMatkul;
        private RecyclerView rvDataMatkul;
        Matkul mk;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKodeMatkul = itemView.findViewById(R.id.tvKodeMatkul);
            tvNamaMatkul = itemView.findViewById(R.id.tvNamaMatkul);
            tvHariMatkul = itemView.findViewById(R.id.tvHariMatkul);
            tvSesiMatkul = itemView.findViewById(R.id.tvSesiMatkul);
            tvSksMatkul = itemView.findViewById(R.id.tvSksMatkul);
            rvDataMatkul = itemView.findViewById(R.id.rvDataMatkul);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), UpdateMatkulActivity.class);
                    intentUpdate.putExtra("kode",mk.getKode());
                    intentUpdate.putExtra("nama",mk.getNama());
                    intentUpdate.putExtra("hari",String.valueOf(mk.getHari()));
                    intentUpdate.putExtra("sesi",String.valueOf(mk.getSesi()));
                    intentUpdate.putExtra("sks",String.valueOf(mk.getSks()));
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }
}
