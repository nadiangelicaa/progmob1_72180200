package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.Dosen.UpdateDosenActivity;
import ukdw.com.progmob_2020.TugasTts.Mahasiswa.UpdateMahasiswaActivity;

public class DsnRecyclerAdapter extends RecyclerView.Adapter<DsnRecyclerAdapter.ViewHolder>{
    private Context context;
    private List<Dosen> dosenList;

    public DsnRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    //tambah konstruktor
    public DsnRecyclerAdapter(List<Dosen> dosenList){
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DsnRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_dsn,parent,false);
        return new DsnRecyclerAdapter.ViewHolder(v); //utk menampilkan viewholder di dlm recyclerview nya
    }

    @Override
    public void onBindViewHolder(@NonNull DsnRecyclerAdapter.ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);
        holder.tvNamaDsn.setText(d.getNama());
        holder.tvNidnDsn.setText(d.getNidn());
        holder.tvAlamatDsn.setText(d.getAlamat());
        holder.tvEmailDsn.setText(d.getEmail());
        holder.tvGelarDsn.setText(d.getGelar());
        holder.d = d;
    }

    @Override
    public int getItemCount() {
        //return 0;
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaDsn, tvNidnDsn, tvAlamatDsn, tvEmailDsn, tvGelarDsn;
        private RecyclerView rvDataDosen;
        Dosen d;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNamaDsn = itemView.findViewById(R.id.tvNamaDsn);
            tvNidnDsn = itemView.findViewById(R.id.tvNidnDsn);
            tvAlamatDsn = itemView.findViewById(R.id.tvAlamatDsn);
            tvEmailDsn = itemView.findViewById(R.id.tvEmailDsn);
            tvGelarDsn = itemView.findViewById(R.id.tvGelarDsn);
            rvDataDosen = itemView.findViewById(R.id.rvDataDosen);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), UpdateDosenActivity.class);
                    intentUpdate.putExtra("nama",d.getNama());
                    intentUpdate.putExtra("nidn",d.getNidn());
                    intentUpdate.putExtra("alamat",d.getAlamat());
                    intentUpdate.putExtra("email",d.getEmail());
                    intentUpdate.putExtra("gelar",d.getGelar());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    //intentUpdate.putExtra("foto",m.getFoto());
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }
}
