package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Crud.MahasiswaUpdateActivity;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.Mahasiswa.UpdateMahasiswaActivity;

public class MhsRecyclerAdapter extends RecyclerView.Adapter<MhsRecyclerAdapter.ViewHolder>{

    private Context context;
    private List<Mahasiswa> mahasiswaList;

    public MhsRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    //tambah konstruktor
    public MhsRecyclerAdapter(List<Mahasiswa> mahasiswaList){
        this.mahasiswaList = mahasiswaList;
    }

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_mhs,parent,false);
        return new ViewHolder(v); //utk menampilkan viewholder di dlm recyclerview nya
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);
        holder.tvNamaMhs.setText(m.getNama());
        holder.tvNimMhs.setText(m.getNim());
        holder.tvAlamatMhs.setText(m.getAlamat());
        holder.tvEmailMhs.setText(m.getEmail());
        holder.m = m;
    }

    @Override
    public int getItemCount() {
        //return 0;
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaMhs, tvNimMhs, tvAlamatMhs, tvEmailMhs;
        private RecyclerView rvDataMahasiswa;
        Mahasiswa m;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNamaMhs = itemView.findViewById(R.id.tvNamaMhs);
            tvNimMhs = itemView.findViewById(R.id.tvNimMhs);
            tvAlamatMhs = itemView.findViewById(R.id.tvAlamatMhs);
            tvEmailMhs = itemView.findViewById(R.id.tvEmailMhs);
            rvDataMahasiswa = itemView.findViewById(R.id.rvDataMahasiswa);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), UpdateMahasiswaActivity.class);
                    intentUpdate.putExtra("nama",m.getNama());
                    intentUpdate.putExtra("nim",m.getNim());
                    //intentUpdate.putExtra("nim_cari",m.getNim_cari());
                    intentUpdate.putExtra("alamat",m.getAlamat());
                    intentUpdate.putExtra("email",m.getEmail());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    //intentUpdate.putExtra("foto",m.getFoto());
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }
}
