package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Crud.MahasiswaUpdateActivity;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Mahasiswa> mahasiswaList;
    //tambahan utk nim yg dicari
    //private DefaultResult mahasiswaListDicari;

    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    //tambah konstruktor
    public MahasiswaCRUDRecyclerAdapter(List<Mahasiswa> mahasiswaList){
        this.mahasiswaList = mahasiswaList;
    }

    //tambahan utk nim yg dicari
    //public MahasiswaCRUDRecyclerAdapter(DefaultResult mahasiswaListDicari){
        //this.mahasiswaListDicari = mahasiswaListDicari;
    //}

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //return null;
        //untuk recycler view
        //View v = LayoutInflater.from(context).inflate(R.layout.item_list_recycler,parent,false);
        //untuk card view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v); //utk menampilkan viewholder di dlm recyclerview nya
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);

        holder.tvNama.setText(m.getNama());
        holder.tvNim.setText(m.getNim());
        //holder.tvNimCari.setText(m.getNim_cari());
        //holder.tvNoTelp.setText(m.getNoTelp());
        holder.tvAlamat.setText(m.getAlamat());
        holder.tvEmail.setText(m.getEmail());
        //holder.tvFoto.setText(m.getFoto());
        //holder.tvNimProgmob.setText(m.getNim_progmob());
        holder.m = m;
    }

    @Override
    public int getItemCount() {
        //return 0;
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNim, tvNoTelp, tvAlamat, tvEmail, tvFoto, tvNimProgmob, tvNimCari;
        private RecyclerView rvMhsUpdate;
        Mahasiswa m;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNim = itemView.findViewById(R.id.tvNim);
            //tvNoTelp = itemView.findViewById(R.id.tvNoTelp);
            //tvNimCari = itemView.findViewById(R.id.tvNimCari);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            //tvFoto = itemView.findViewById(R.id.tvFoto);
            //tvNimProgmob = itemView.findViewById(R.id.tvNimProgmob);
            rvMhsUpdate = itemView.findViewById(R.id.rvGetMhsAll_Delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), MahasiswaUpdateActivity.class);
                    intentUpdate.putExtra("nama",m.getNama());
                    intentUpdate.putExtra("nim",m.getNim());
                    //intentUpdate.putExtra("nim_cari",m.getNim_cari());
                    intentUpdate.putExtra("alamat",m.getAlamat());
                    intentUpdate.putExtra("email",m.getEmail());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    //intentUpdate.putExtra("foto",m.getFoto());
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }
}
