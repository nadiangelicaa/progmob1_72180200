package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Jadwal;
import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.TugasTts.Jadwal.UpdateJadwalActivity;
import ukdw.com.progmob_2020.TugasTts.Matkul.UpdateMatkulActivity;

public class JadwalRecyclerAdapter extends RecyclerView.Adapter<JadwalRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Jadwal> jadwalList;

    public JadwalRecyclerAdapter(Context context) {
        this.context = context;
        jadwalList = new ArrayList<>();
    }

    //tambah konstruktor
    public JadwalRecyclerAdapter(List<Jadwal> jadwalList){
        this.jadwalList = jadwalList;
    }

    public List<Jadwal> getJadwalList() {
        return jadwalList;
    }

    public void setJadwalListList(List<Jadwal> jadwalList) {
        this.jadwalList = jadwalList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public JadwalRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_jadwal,parent,false);
        return new ViewHolder(v); //utk menampilkan viewholder di dlm recyclerview nya
    }

    @Override
    public void onBindViewHolder(@NonNull JadwalRecyclerAdapter.ViewHolder holder, int position) {
        Jadwal j = jadwalList.get(position);
        holder.tvNamaMatkulJadwal.setText(j.getMatkul());
        holder.tvNamaDosenJadwal.setText(j.getDosen());
        holder.tvNidnJadwal.setText(j.getNidn());
        holder.tvHariMatkulJadwal.setText(j.getHari());
        holder.tvSesiMatkulJadwal.setText(j.getSesi());
        holder.tvSksMatkulJadwal.setText(j.getSks());
        holder.j = j;
    }

    @Override
    public int getItemCount() {
        //return 0;
        return jadwalList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNamaMatkulJadwal, tvNamaDosenJadwal, tvNidnJadwal, tvHariMatkulJadwal, tvSesiMatkulJadwal, tvSksMatkulJadwal;
        private RecyclerView rvDataJadwal;
        Jadwal j;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNamaMatkulJadwal = itemView.findViewById(R.id.tvNamaMatkulJadwal);
            tvNamaDosenJadwal = itemView.findViewById(R.id.tvNamaDosenJadwal);
            tvNidnJadwal = itemView.findViewById(R.id.tvNidnDosenJadwal);
            tvHariMatkulJadwal = itemView.findViewById(R.id.tvHariMatkulJadwal);
            tvSesiMatkulJadwal = itemView.findViewById(R.id.tvSesiMatkulJadwal);
            tvSksMatkulJadwal = itemView.findViewById(R.id.tvSksMatkulJadwal);
            rvDataJadwal = itemView.findViewById(R.id.rvDataJadwal);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), UpdateJadwalActivity.class);
                    intentUpdate.putExtra("matkul",j.getMatkul());
                    intentUpdate.putExtra("dosen",j.getDosen());
                    intentUpdate.putExtra("nidn",j.getNidn());
                    intentUpdate.putExtra("hari",j.getHari());
                    intentUpdate.putExtra("sesi",j.getSesi());
                    intentUpdate.putExtra("sks",j.getSks());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }
}
