package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ukdw.com.progmob_2020.R;

public class MainTtsProgmobAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private String[] numberWord;
    private int[] numberImage;

    public MainTtsProgmobAdapter(Context c,String[] numberWord,int[] numberImage){
        context = c;
        this.numberWord = numberWord;
        this.numberImage = numberImage;
    }

    @Override
    public int getCount() {
        //return 0;
        return numberWord.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.row_item, null);
        }
        ImageView imgMenuMainTtsProgmob = convertView.findViewById(R.id.imgMenuMainTtsProgmob);
        TextView txtMenuMainTtsProgmob = convertView.findViewById(R.id.txtMenuMainTtsProgmob);

        imgMenuMainTtsProgmob.setImageResource(numberImage[position]);
        txtMenuMainTtsProgmob.setText(numberWord[position]);

        return convertView;
    }
}
