package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);

        Button btnGetAllMhs = (Button)findViewById(R.id.buttonGetMhs);
        btnGetAllMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGetAllMhs = new Intent(MainMhsActivity.this,MahasiswaGetAllActivity.class);
                startActivity(intentGetAllMhs);
            }
        });

        Button btnAddMhs = (Button)findViewById(R.id.buttonAddMhs);
        btnAddMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAddMhs = new Intent(MainMhsActivity.this,MahasiswaAddActivity.class);
                startActivity(intentAddMhs) ;
            }
        });

        Button btnUpdateMhs = (Button)findViewById(R.id.buttonUpdateMhs);
        btnUpdateMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intentUpdateMhs = new Intent(MainMhsActivity.this, MahasiswaUpdateActivity.class);
                Intent intentUpdateMhs = new Intent(MainMhsActivity.this, MahasiswaGetAllActivity.class);
                startActivity(intentUpdateMhs);
            }
        });

        Button btnDeleteMhs = (Button)findViewById(R.id.buttonDeleteMhs);
        btnDeleteMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDeleteMhs = new Intent(MainMhsActivity.this, HapusMhsActivity.class);
                startActivity(intentDeleteMhs);
            }
        });
    }
}