package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.progmob_2020.HelpActivity;
import ukdw.com.progmob_2020.MainActivity;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class SearchActivity extends AppCompatActivity {
    RecyclerView rvMhsNimDicari;
    MahasiswaCRUDRecyclerAdapter mhsAdapter;
    ProgressDialog pd;
    List<Mahasiswa> mahasiswaListDicari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //binding
        /*TextView nimDicari = (TextView)findViewById(R.id.nimDicari);
        //ambil data nim yg dicari sesuai edittext
        Bundle b = getIntent().getExtras();
        String textNim = b.getString("searchString");
        nimDicari.setText(textNim);

        Button btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, MahasiswaUpdateActivity.class);
                Bundle b = new Bundle();
                b.putString("nimYangDicari",nimDicari.getText().toString());
                intent.putExtras(b);
                startActivity(intent);
            }
        });*/

        //tampilin data dlm rv
        rvMhsNimDicari = (RecyclerView)findViewById(R.id.rvUpdateMhs);
        pd = new ProgressDialog(this);
        pd.setTitle("Data Sedang Dicari...");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Mahasiswa>> call = service.getMahasiswa("72180200");

        call.enqueue(new Callback<List<Mahasiswa>>() {
            @Override
            public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                pd.dismiss();
                mahasiswaListDicari = response.body();
                mhsAdapter = new MahasiswaCRUDRecyclerAdapter(mahasiswaListDicari);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SearchActivity.this);
                rvMhsNimDicari.setLayoutManager(layoutManager);
                rvMhsNimDicari.setAdapter(mhsAdapter);
            }

            @Override
            public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SearchActivity.this,"Error",Toast.LENGTH_LONG).show();
            }
        });

        rvMhsNimDicari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDataYangAkanDiupdate = new Intent(SearchActivity.this,MahasiswaUpdateActivity.class);
                //Bundle b = new Bundle();
                //intentDataYangAkanDiupdate.putExtras(b);
                startActivity(intentDataYangAkanDiupdate);
            }
        });

        //nim yg dicari dijadikan string utk dimasukkan ke dataservice
        //String nimMhsYangDicari = nimDicari.getText().toString();

        //tampilin data dlm rv
        /*rvMhsNimDicari = (RecyclerView)findViewById(R.id.rvUpdateMhs);
        pd = new ProgressDialog(this);
        pd.setTitle("Data Sedang Dicari");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<DefaultResult> call = service.get_mhs_nim_dicari(nimMhsYangDicari,"72180200");

        call.enqueue(new Callback<DefaultResult>() {
            @Override
            public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                pd.dismiss();
                mahasiswaListDicari = response.body();
                mhsAdapter = new MahasiswaCRUDRecyclerAdapter(mahasiswaListDicari);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SearchActivity.this);
                rvMhsNimDicari.setLayoutManager(layoutManager);
                rvMhsNimDicari.setAdapter(mhsAdapter);
            }

            @Override
            public void onFailure(Call<DefaultResult> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SearchActivity.this,"Error",Toast.LENGTH_LONG).show();
            }
        });*/
    }
}