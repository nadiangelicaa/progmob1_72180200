package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class HapusMhsActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_mhs);

        EditText edNimHapus = (EditText)findViewById(R.id.editTextNim);
        edNimHapus.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    Toast.makeText(HapusMhsActivity.this, "NIM Yang Akan Dihapus : " +
                            edNimHapus.getText(), Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });
        //String nimYangAkanDihapus = edNimHapus.getText().toString();
        //ambil data nim yg dicari sesuai edittext
        //Intent data = getIntent();
        //String idData = data.getStringExtra("nim");

        Button btnHapus = (Button)findViewById(R.id.buttonHapusMhs);
        pd = new ProgressDialog(HapusMhsActivity.this);


        /*if (data.getExtras() != null){
            edNimHapus.setText(data.getStringExtra("nim"));
        }*/

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.setCancelable(false);
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_mhs(
                        edNimHapus.getText().toString(),
                        "72180200"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusMhsActivity.this, "Data Berhasil Dihapus", Toast.LENGTH_LONG).show();
                        Intent intentHapusData = new Intent(HapusMhsActivity.this, MainMhsActivity.class);
                        startActivity(intentHapusData);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusMhsActivity.this, "Gagal/ Data Tidak Tersedia", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        //jika nim yg akan dihapus ada data nimnya, baru muncul toast terhapus, klo gk muncul toast gagal/ data tidak tersedia
        //if(nimYangAkanDihapus == idData) {
        //}
    }
}