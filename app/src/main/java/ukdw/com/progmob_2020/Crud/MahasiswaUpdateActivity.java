package ukdw.com.progmob_2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.HelpActivity;
import ukdw.com.progmob_2020.MainActivity;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MahasiswaUpdateActivity extends AppCompatActivity {

    ProgressDialog pd;
    //public static final String NIM_KEY = "nim";
    //private EditText edNimCari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        //mengambil teks dari edittext (bind)
        /*edNimCari = (EditText)findViewById(R.id.editTextNimCari);
        edNimCari.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    Toast.makeText(MahasiswaUpdateActivity.this, "NIM Yang Dicari " + edNimCari.getText(), Toast.LENGTH_SHORT).show();
                    //kirim data dgn bundle ke search activity
                    Intent nimYangDicari = new Intent(MahasiswaUpdateActivity.this, SearchActivity.class);
                    Bundle b = new Bundle();
                    b.putString("searchString",edNimCari.getText().toString());
                    nimYangDicari.putExtras(b);
                    startActivity(nimYangDicari);
                    return true;
                }
                return false;
            }
        });*/
        /*edNimCari.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    Toast.makeText(MahasiswaUpdateActivity.this, "NIM Yang Dicari " + edNimCari.getText(), Toast.LENGTH_SHORT).show();
                    //kirim data dgn bundle ke search activity
                    Intent nimYangDicari = new Intent(MahasiswaUpdateActivity.this, SearchActivity.class);
                    Bundle b = new Bundle();
                    b.putString("searchString",edNimCari.getText().toString());
                    nimYangDicari.putExtras(b);
                    startActivity(nimYangDicari);
                    return true;
                }
                return false;
            }
        });*/

        EditText edNama2 = (EditText) findViewById(R.id.editTextNama2);
        EditText edNim2 = (EditText) findViewById(R.id.editTextNim2);
        EditText edNimCari = (EditText) findViewById(R.id.editTextNimCari);
        EditText edAlamat2 = (EditText) findViewById(R.id.editTextAlamat2);
        EditText edEmail2 = (EditText) findViewById(R.id.editTextEmail2);
        Button btnUpdate = (Button) findViewById(R.id.buttonUbahMhs);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        //ambil data nim yg dicari sesuai edittext
        //Bundle b = getIntent().getExtras();
        Intent data = getIntent();
        //String nimYangDicari = data.getStringExtra("nim");
        if (data.getExtras() != null) {
            edNama2.setText(data.getStringExtra("nama"));
            edNim2.setText(data.getStringExtra("nim"));
            edNimCari.setText(data.getStringExtra("nim"));
            edAlamat2.setText(data.getStringExtra("alamat"));
            edEmail2.setText(data.getStringExtra("email"));

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Mohon Ditunggu...");
                    pd.setCancelable(false);
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> call = service.update_mhs(
                            edNama2.getText().toString(),
                            edNim2.getText().toString(),
                            edNimCari.getText().toString(),
                            edAlamat2.getText().toString(),
                            edEmail2.getText().toString(),
                            "Kosongkan saja diisi sembarang karena dirandom sistem",
                            "72180200"
                    );

                    call.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(MahasiswaUpdateActivity.this, "Data Berhasil Diupdate", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(MahasiswaUpdateActivity.this, "Update Data Gagal", Toast.LENGTH_LONG).show();
                        }
                    });

                    Intent intentUpdateMhs = new Intent(MahasiswaUpdateActivity.this,MainMhsActivity.class);
                    startActivity(intentUpdateMhs);
                }
            });
        }

    }
}