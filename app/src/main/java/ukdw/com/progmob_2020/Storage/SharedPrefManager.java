package ukdw.com.progmob_2020.Storage;

import android.content.Context;
import android.content.SharedPreferences;

import ukdw.com.progmob_2020.Model.User;

public class SharedPrefManager {
    public static final String SHARED_PREF_NAME = "my_shared_pref";

    //untuk menentukan apakah sdh login atau belum
    public static String IS_LOGGED_IN = "isLoggedIn";

    private static SharedPrefManager mInstance;
    public Context context;

     private SharedPrefManager(Context context){
         this.context = context;
     }

     public static synchronized SharedPrefManager getInstance(Context context){
         if (mInstance == null){
             mInstance = new SharedPrefManager(context);
         }
         return mInstance;
     }

     public void saveUser (User user){
         SharedPreferences pref = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
         SharedPreferences.Editor editor = pref.edit();

         editor.putString("id",user.getId());
         editor.putString("nama",user.getNama());
         editor.putString("nimnik",user.getNimnik());
         editor.putString("email",user.getEmail());
         editor.putString("password",user.getPassword());
         editor.putString("is_admin",user.getIs_admin());

         editor.putBoolean(IS_LOGGED_IN, true);

         editor.apply();
     }

     public boolean isLoggedIn(){
         SharedPreferences pref = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
         return pref.getBoolean(IS_LOGGED_IN, false);
         /*String isLogin = pref.getString("id","0");
         if (isLogin.equals("0")){
             return true; //kalau nilainya 1 berarti user udah login
         }
         return false; //kalau nilainya 0 (defaut values) brti blm login*/
     }

     public User getUser(){
         SharedPreferences pref = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
         User user =  new User(
                 pref.getString("id","0"),
                 pref.getString("nama",null),
                 pref.getString("nimnik",null),
                 pref.getString("email",null),
                 pref.getString("password",null),
                 pref.getString("is_admin","1")
         );
         return user;
     }

     //untuk logout
    public void clear(){
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
    }
}
