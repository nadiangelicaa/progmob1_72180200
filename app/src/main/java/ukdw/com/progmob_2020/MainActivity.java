package ukdw.com.progmob_2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ukdw.com.progmob_2020.Pertemuan3.CardActivity;
import ukdw.com.progmob_2020.Pertemuan3.CardViewTestActivity;
import ukdw.com.progmob_2020.Pertemuan3.ListActivity;
import ukdw.com.progmob_2020.Pertemuan3.RecyclerActivity;
import ukdw.com.progmob_2020.Pertemuan4.DebuggingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.test_constraint_layout);
        //setContentView(R.layout.test_linear_layout);
        //setContentView(R.layout.test_table_layout);
        //setContentView(R.layout.activity_tracker);
        setContentView(R.layout.activity_main);

        //variabel
        final TextView txtView = (TextView)findViewById(R.id.mainActivityTextView);
        //untuk mendapatkan textview yang ada di layout, utk menghubungkan kls java & layout
        Button myBtn = (Button)findViewById(R.id.button1);
        final EditText myEditText = (EditText)findViewById(R.id.editText1);
        Button btnHelp = (Button)findViewById(R.id.btnHelp);
        Button btnTrackerLayout = (Button)findViewById(R.id.btnTracker);

        //pertemuan 3
        Button btnList = (Button)findViewById(R.id.buttonListView);
        Button btnRecycler = (Button)findViewById(R.id.buttonRecyclerView);
        Button btnCard = (Button)findViewById(R.id.buttonCardView);
        Button btnCvTugas = (Button)findViewById(R.id.btnCardViewTugas);

        //pertemuan 4
        Button btnDebug = (Button)findViewById(R.id.btnDebugging);

        //action
        txtView.setText(R.string.text_hello_world);
        //berguna kalau mau buat judul/ button yang butuh konsistensi

        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("COBA KLIIKKKKKKKKK",myEditText.getText().toString());
                txtView.setText(myEditText.getText().toString());
            }
        });

        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,HelpActivity.class);
                //inten untuk berpindah antar activity, parameter pertama intent sblmnya
                //parameter kedua untuk activity yg dituju
                Bundle b = new Bundle();

                b.putString("helpString",myEditText.getText().toString());
                //mengirim string ke helpactivity lewat button help
                //agar string yg dikirim sama spt yang ada di edittext di main activity
                intent.putExtras(b);

                startActivity(intent);
            }
        });

        btnTrackerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTracker = new Intent(MainActivity.this,TrackerActivity.class);
                startActivity(intentTracker);
            }
        });

        //pertemuan 3
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentList = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intentList);
            }
        });

        btnRecycler.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intentRecycler = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(intentRecycler);
            }
        });

        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCard = new Intent(MainActivity.this, CardActivity.class);
                startActivity(intentCard);
            }
        });

        btnCvTugas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCvTugas = new Intent(MainActivity.this, CardViewTestActivity.class);
                startActivity(intentCvTugas);
            }
        });

        //pertemuan 4
        btnDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDebug = new Intent(MainActivity.this, DebuggingActivity.class);
                startActivity(intentDebug);
            }
        });
    }
}